package com.example.realmdb.testrealmdb.adapter;

import android.content.Context;

import com.example.realmdb.testrealmdb.models.Book;

import io.realm.RealmResults;

/**
 * Created by Siwat on 14/7/2559.
 */

public class RealmBooksAdapter extends RealmModelAdapter<Book> {

    public RealmBooksAdapter(Context context, RealmResults<Book> realmResults, boolean automaticUpdate) {

        super(context, realmResults, automaticUpdate);
    }
}
